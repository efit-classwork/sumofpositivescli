public class SumOfPositivesCLI {

    public static void main(String[] args){

        int count = args.length;
        int total = 0;

        for(int i = 0; i<count; i++){

            if(Integer.parseInt(args[i]) > 0){
                total = total + Integer.parseInt(args[i]);

            }

        }
        System.out.println(total);
    }
}
